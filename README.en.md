# spider-rule

#### Description
一款灵活且性能高效的规则引擎，采用Mysql+Redis+匹配算法实现。
灵感来源于Drools，但Drools将数据放到服务内存中，当数据量很大，服务所需要的内存空间也相当大，且不管有没有用到都需要加载到内存中，而且每次加载很慢。
因此设计一款将数据放到Mysql和Redis中，Mysql存储发布的规则数据，Redis存储最近使用的规则数据。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
