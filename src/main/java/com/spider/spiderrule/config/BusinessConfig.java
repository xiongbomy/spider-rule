package com.spider.spiderrule.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 8:45 下午
 */

@Data
@Component
public class BusinessConfig {

    @Value("${tenant-id}")
    private String tenantId;

}
