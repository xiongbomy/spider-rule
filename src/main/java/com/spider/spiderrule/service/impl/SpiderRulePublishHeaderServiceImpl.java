package com.spider.spiderrule.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spider.spiderrule.mapper.SpiderRulePublishHeaderMapper;
import com.spider.spiderrule.model.entity.SpiderRulePublishHeader;
import com.spider.spiderrule.service.SpiderRulePublishHeaderService;
import org.springframework.stereotype.Service;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:51 下午
 */

@Service
public class SpiderRulePublishHeaderServiceImpl extends ServiceImpl<SpiderRulePublishHeaderMapper, SpiderRulePublishHeader>
        implements SpiderRulePublishHeaderService {

    @Override
    public int deleteByRuleId(Long ruleId) {
        return this.baseMapper.deleteByRuleId(ruleId);
    }
}
