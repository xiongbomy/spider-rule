package com.spider.spiderrule.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spider.spiderrule.enums.RuleDataStatusEnum;
import com.spider.spiderrule.exception.BusinessException;
import com.spider.spiderrule.mapper.SpiderRuleDeployMapper;
import com.spider.spiderrule.mapper.SpiderRuleMapper;
import com.spider.spiderrule.model.entity.SpiderRule;
import com.spider.spiderrule.model.entity.SpiderRuleDeploy;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishHeader;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishItem;
import com.spider.spiderrule.model.vo.RuleHisPublishDataVo;
import com.spider.spiderrule.service.SpiderRuleDeployService;
import com.spider.spiderrule.service.SpiderRuleHisPublishHeaderService;
import com.spider.spiderrule.service.SpiderRuleHisPublishItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:32 下午
 */

@Service
public class SpiderRuleDeployServiceImpl extends ServiceImpl<SpiderRuleDeployMapper, SpiderRuleDeploy>
        implements SpiderRuleDeployService {

    @Autowired
    private SpiderRuleMapper ruleMapper;
    @Autowired
    private SpiderRuleHisPublishHeaderService hisPublishHeaderService;
    @Autowired
    private SpiderRuleHisPublishItemService hisPublishItemService;

    @Override
    public int selectLatestVersion(Long ruleId) {
        return this.baseMapper.selectLatestVersion(ruleId);
    }

    @Override
    public IPage<SpiderRuleDeploy> selectPage(Long ruleId, int page, int pageSize) {
        Page<SpiderRuleDeploy> pageObject = new Page(page, pageSize);
        LambdaQueryWrapper<SpiderRuleDeploy> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SpiderRuleDeploy::getRuleId, ruleId);
        queryWrapper.orderByDesc(SpiderRuleDeploy::getCreateTime);
        return this.baseMapper.selectPage(pageObject, queryWrapper);
    }

    @Override
    public RuleHisPublishDataVo selectHisPublishList(Long ruleId) {

        SpiderRule rule = ruleMapper.selectById(ruleId);
        if (rule == null || RuleDataStatusEnum.PUBLISH.getCode() != rule.getDataStatus()) {
            throw new BusinessException(String.format("规则未发布,ruleId:【%s】", ruleId));
        }

        LambdaQueryWrapper<SpiderRuleHisPublishHeader> hisHeaderQueryWrapper = new LambdaQueryWrapper<>();
        hisHeaderQueryWrapper.eq(SpiderRuleHisPublishHeader::getRuleId, ruleId);
        hisHeaderQueryWrapper.orderByDesc(SpiderRuleHisPublishHeader::getCreateTime);
        List<SpiderRuleHisPublishHeader> hisHeaderList = hisPublishHeaderService.list(hisHeaderQueryWrapper);

        LambdaQueryWrapper<SpiderRuleHisPublishItem> hisItemQueryWrapper = new LambdaQueryWrapper<>();
        hisItemQueryWrapper.eq(SpiderRuleHisPublishItem::getRuleId, ruleId);
        hisItemQueryWrapper.orderByDesc(SpiderRuleHisPublishItem::getCreateTime);
        List<SpiderRuleHisPublishItem> hisItemList = hisPublishItemService.list(hisItemQueryWrapper);

        RuleHisPublishDataVo result = new RuleHisPublishDataVo();
        result.setHisPublishHeaderList(hisHeaderList);
        result.setHisPublishItemList(hisItemList);

        return result;
    }
}
