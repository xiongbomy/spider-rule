package com.spider.spiderrule.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spider.spiderrule.mapper.SpiderRulePublishItemMapper;
import com.spider.spiderrule.model.entity.SpiderRulePublishItem;
import com.spider.spiderrule.service.SpiderRulePublishItemService;
import org.springframework.stereotype.Service;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:56 下午
 */

@Service
public class SpiderRulePublishItemServiceImpl extends ServiceImpl<SpiderRulePublishItemMapper, SpiderRulePublishItem>
        implements SpiderRulePublishItemService {

    @Override
    public int deleteByRuleId(Long ruleId) {
        return this.baseMapper.deleteByRuleId(ruleId);
    }
}
