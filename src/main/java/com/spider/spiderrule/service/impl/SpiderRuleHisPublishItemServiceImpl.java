package com.spider.spiderrule.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spider.spiderrule.mapper.SpiderRuleHisPublishItemMapper;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishItem;
import com.spider.spiderrule.service.SpiderRuleHisPublishItemService;
import org.springframework.stereotype.Service;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:34 下午
 */
@Service
public class SpiderRuleHisPublishItemServiceImpl extends ServiceImpl<SpiderRuleHisPublishItemMapper, SpiderRuleHisPublishItem>
        implements SpiderRuleHisPublishItemService {
}
