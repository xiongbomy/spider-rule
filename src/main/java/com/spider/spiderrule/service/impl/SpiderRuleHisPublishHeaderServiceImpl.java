package com.spider.spiderrule.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spider.spiderrule.mapper.SpiderRuleHisPublishHeaderMapper;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishHeader;
import com.spider.spiderrule.service.SpiderRuleHisPublishHeaderService;
import org.springframework.stereotype.Service;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:29 下午
 */

@Service
public class SpiderRuleHisPublishHeaderServiceImpl extends ServiceImpl<SpiderRuleHisPublishHeaderMapper, SpiderRuleHisPublishHeader>
        implements SpiderRuleHisPublishHeaderService {
}
