package com.spider.spiderrule.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spider.spiderrule.exception.BusinessException;
import com.spider.spiderrule.mapper.SpiderRuleEditItemMapper;
import com.spider.spiderrule.model.entity.SpiderRuleEditItem;
import com.spider.spiderrule.service.SpiderRuleEditItemService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Collections;
import java.util.List;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:23 下午
 */

@Service
public class SpiderRuleEditItemServiceImpl extends ServiceImpl<SpiderRuleEditItemMapper, SpiderRuleEditItem>
        implements SpiderRuleEditItemService {


    @Override
    public IPage<SpiderRuleEditItem> selectPage(Long ruleId, int page, int pageSize) {
        Page<SpiderRuleEditItem> pageObject = new Page(page, pageSize);
        LambdaQueryWrapper<SpiderRuleEditItem> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SpiderRuleEditItem::getRuleId, ruleId);
        queryWrapper.orderByDesc(SpiderRuleEditItem::getCreateTime);
        return this.baseMapper.selectPage(pageObject, queryWrapper);
    }

    @Override
    public SpiderRuleEditItem findById(Long id) {
        SpiderRuleEditItem editItem = this.getById(id);
        if (editItem == null) {
            throw new BusinessException("该规则矩阵表行不存在,id:【" + id + "】");
        }
        return editItem;
    }

    @Override
    public List<SpiderRuleEditItem> batchSaveRuleEditHeader(List<SpiderRuleEditItem> ruleEditItems) {
        if (ObjectUtils.isEmpty(ruleEditItems)) {
            return Collections.emptyList();
        }
        this.saveBatch(ruleEditItems, ruleEditItems.size());
        return ruleEditItems;
    }

    @Override
    public List<SpiderRuleEditItem> batchEditRuleEditHeader(List<SpiderRuleEditItem> ruleEditItems) {
        this.updateBatchById(ruleEditItems, ruleEditItems.size());
        return ruleEditItems;
    }

    @Override
    public Boolean deleteRuleEditHeader(List<Long> ids) {
        this.baseMapper.deleteBatchIds(ids);
        return true;
    }
}
