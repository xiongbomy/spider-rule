package com.spider.spiderrule.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.spider.spiderrule.model.entity.SpiderRuleDeploy;
import com.spider.spiderrule.model.vo.RuleHisPublishDataVo;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:31 下午
 */


public interface SpiderRuleDeployService extends IService<SpiderRuleDeploy> {

    /**
     * 查询某个规则的最新版本
     *
     * @param ruleId 规则ID
     * @return
     */
    int selectLatestVersion(Long ruleId);

    IPage<SpiderRuleDeploy> selectPage(Long ruleId, int page, int pageSize);

    /**
     * 查询某个规则的历史发布规则数据
     *
     * @param ruleId
     * @return
     */
    RuleHisPublishDataVo selectHisPublishList(Long ruleId);
}
