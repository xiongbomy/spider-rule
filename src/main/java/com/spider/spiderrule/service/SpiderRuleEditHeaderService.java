package com.spider.spiderrule.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.spider.spiderrule.model.entity.SpiderRuleEditHeader;

import java.util.List;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:20 下午
 */
public interface SpiderRuleEditHeaderService extends IService<SpiderRuleEditHeader> {

    IPage<SpiderRuleEditHeader> selectPage(Long ruleId, int page, int pageSize);

    SpiderRuleEditHeader findById(Long id);

    List<SpiderRuleEditHeader> batchSaveRuleEditHeader(List<SpiderRuleEditHeader> ruleEditHeaders);

    List<SpiderRuleEditHeader> batchEditRuleEditHeader(List<SpiderRuleEditHeader> ruleEditHeaders);

    Boolean deleteRuleEditHeader(List<Long> ids);

}
