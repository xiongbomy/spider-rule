package com.spider.spiderrule.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/21 11:04 上午
 */
public interface SpiderRuleMatchApiService {


    /**
     * 匹配规则
     *
     * @param ruleCode  规则编码
     * @param parameter 入参对象
     * @return
     */
    JSONObject matchRuleByRuleCode(String ruleCode, JSONObject parameter);

}
