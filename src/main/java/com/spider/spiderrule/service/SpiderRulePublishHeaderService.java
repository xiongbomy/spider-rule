package com.spider.spiderrule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spider.spiderrule.model.entity.SpiderRulePublishHeader;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:49 下午
 */

public interface SpiderRulePublishHeaderService extends IService<SpiderRulePublishHeader> {

    int deleteByRuleId(Long ruleId);

}
