package com.spider.spiderrule.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.spider.spiderrule.model.entity.SpiderRule;
import com.spider.spiderrule.model.vo.RuleRedisConditionVo;


/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:27 下午
 */
public interface SpiderRuleService extends IService<SpiderRule> {

    IPage<SpiderRule> selectPage(int page, int pageSize);

    SpiderRule findById(Long id);

    SpiderRule findByRuleCode(String ruleCode);

    SpiderRule saveRule(SpiderRule spiderRule);

    SpiderRule editRule(SpiderRule spiderRule);

    Boolean deleteRule(Long id);

    Boolean invalidRule(Long id);

    Boolean unInvalidRule(Long id);

    Boolean releaseRule(Long id);

    RuleRedisConditionVo findRulePublishFromRedis(String ruleCode, String type, String tenantId);

}
