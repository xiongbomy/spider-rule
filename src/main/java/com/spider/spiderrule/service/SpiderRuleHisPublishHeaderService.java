package com.spider.spiderrule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishHeader;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:29 下午
 */

public interface SpiderRuleHisPublishHeaderService extends IService<SpiderRuleHisPublishHeader> {
}
