package com.spider.spiderrule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishItem;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:34 下午
 */
public interface SpiderRuleHisPublishItemService extends IService<SpiderRuleHisPublishItem> {
}
