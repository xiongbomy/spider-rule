package com.spider.spiderrule.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.spider.spiderrule.model.entity.SpiderRuleEditItem;

import java.util.List;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:23 下午
 */
public interface SpiderRuleEditItemService extends IService<SpiderRuleEditItem> {

    IPage<SpiderRuleEditItem> selectPage(Long ruleId, int page, int pageSize);

    SpiderRuleEditItem findById(Long id);

    List<SpiderRuleEditItem> batchSaveRuleEditHeader(List<SpiderRuleEditItem> ruleEditItems);

    List<SpiderRuleEditItem> batchEditRuleEditHeader(List<SpiderRuleEditItem> ruleEditItems);

    Boolean deleteRuleEditHeader(List<Long> ids);
}
