package com.spider.spiderrule.model.vo;

import com.spider.spiderrule.model.entity.SpiderRuleHisPublishHeader;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishItem;
import lombok.Data;

import java.util.List;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/5/4 3:11 下午
 */

@Data
public class RuleHisPublishDataVo {


    private List<SpiderRuleHisPublishHeader> hisPublishHeaderList;

    private List<SpiderRuleHisPublishItem> hisPublishItemList;

}
