package com.spider.spiderrule.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/17 8:25 下午
 */

@Data
public class RulePublishHeaderVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String headerUuid;

    private String fieldCode;

    private String fieldName;

    private Integer headerType;


}
