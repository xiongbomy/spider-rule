package com.spider.spiderrule.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/17 8:26 下午
 */

@Data
public class RulePublishItemVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 租户号
     */
    private String tenantId;

    /**
     * 当前最新版本
     */
    private Integer version;

    /**
     * 头表标识
     */
    private String headerUuid;

    /**
     * 行号
     */
    private Integer rowIndex;

    /**
     * 操作符
     */
    private String operator;

    /**
     * 规则ID
     */
    private Long ruleId;

    /**
     * 规则编码
     */
    private String ruleCode;


}
