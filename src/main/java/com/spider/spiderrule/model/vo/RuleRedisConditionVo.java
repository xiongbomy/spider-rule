package com.spider.spiderrule.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/18 7:40 下午
 */
@Data
public class RuleRedisConditionVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 输入的请求头
     */
    private List<RulePublishHeaderVo> conditionVoList;

    /**
     * 请求头对应的规矩矩阵
     */
    private Map<String, List<RulePublishItemVo>> conditionVoListMap;

}
