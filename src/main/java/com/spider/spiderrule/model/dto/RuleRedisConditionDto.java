package com.spider.spiderrule.model.dto;

import com.spider.spiderrule.model.vo.RulePublishHeaderVo;
import com.spider.spiderrule.model.vo.RulePublishItemVo;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/21 2:55 下午
 */

@Data
public class RuleRedisConditionDto {

    /**
     * 表头集合
     */
    private List<RulePublishHeaderVo> headerList;

    /**
     * 表头对于的表行数据
     * key为表头的uuid
     */
    private Map<String,RulePublishItemVo> itemVoMap;

    /**
     * 当前行数
     */
    private Integer rowIndex;

}
