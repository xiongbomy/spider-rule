package com.spider.spiderrule.model.param;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/21 11:35 上午
 */

@Data
public class RuleMatchParam {

    /**
     * 规则编码
     */
    private String ruleCode;

    /**
     * 输入的参数
     */
    private JSONObject parameter;

}
