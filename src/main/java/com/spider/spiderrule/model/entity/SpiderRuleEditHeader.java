package com.spider.spiderrule.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 编辑的规则矩阵-头表
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:09 下午
 */
@TableName("t_spider_rule_edit_header")
@Data
public class SpiderRuleEditHeader implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 租户号
     */
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 头表标识
     */
    private String headerUuid;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段编码
     */
    private String fieldCode;

    /**
     * 头顺序
     */
    private Integer headerIndex;

    /**
     * 规则编码
     */
    private Long ruleId;

    /**
     * 规则编码
     */
    private String ruleCode;

    /**
     * 类型:1-输入，2-输出，3-分组
     */
    private Integer headerType;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateUser;

    /**
     * update_time
     */
    private Date updateTime;

    /**
     * 删除人
     */
    private String deleteUser;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 是否删除 0未删除 1删除
     */
    private Integer isDelete;

    public SpiderRuleEditHeader() {
    }
}
