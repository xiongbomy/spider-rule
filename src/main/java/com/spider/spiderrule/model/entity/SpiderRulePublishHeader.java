package com.spider.spiderrule.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 发布的规则矩阵-头表
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:47 下午
 */
@TableName("t_spider_rule_publish_header")
@Data
public class SpiderRulePublishHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户号
     */
    private String tenantId;

    /**
     * 数据状态，1发布，2作废
     */
    private Integer dataStatus;

    /**
     * 当前最新版本
     */
    private Integer version;

    /**
     * 头表标识
     */
    private String headerUuid;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段编码
     */
    private String fieldCode;

    /**
     * 头顺序
     */
    private Integer headerIndex;

    /**
     * 规则编码
     */
    private Long ruleId;

    /**
     * 规则编码
     */
    private String ruleCode;

    /**
     * 类型:1-输入，2-输出，3-分组
     */
    private Integer headerType;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateUser;

    /**
     * update_time
     */
    private Date updateTime;

    public SpiderRulePublishHeader() {
    }
}