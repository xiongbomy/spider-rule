package com.spider.spiderrule.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/16 7:28 下午
 */

@Data
public class BaseCreateEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 租户号
     */
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 创建时间
     */
    private Date createTime;

}
