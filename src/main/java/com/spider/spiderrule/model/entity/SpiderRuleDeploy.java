package com.spider.spiderrule.model.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 规则引擎-规则发布记录表
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:09 下午
 */
@TableName("t_spider_rule_deploy")
@Data
public class SpiderRuleDeploy implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 租户号
     */
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 规则表ID
     */
    private Long ruleId;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 创建时间
     */
    private Date createTime;


}