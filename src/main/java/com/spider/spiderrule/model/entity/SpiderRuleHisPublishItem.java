package com.spider.spiderrule.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 历史发布的规则矩阵-行表
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:31 下午
 */
@TableName("t_spider_rule_his_publish_item")
@Data
public class SpiderRuleHisPublishItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户号
     */
    private String tenantId;

    /**
     * 当前最新版本
     */
    private Integer version;

    /**
     * 头表标识
     */
    private String headerUuid;

    /**
     * 行号
     */
    private Integer rowIndex;

    /**
     * 操作符
     */
    private String operator;

    /**
     * 规则ID
     */
    private Long ruleId;

    /**
     * 规则编码
     */
    private String ruleCode;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateUser;

    /**
     * update_time
     */
    private Date updateTime;

    public SpiderRuleHisPublishItem() {}
}