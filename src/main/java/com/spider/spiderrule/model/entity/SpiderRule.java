package com.spider.spiderrule.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 规则引擎-规则表
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:06 下午
 */
@TableName("t_spider_rule")
@Data
public class SpiderRule implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 租户号
     */
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 规则名称
     */
    private String ruleName;

    /**
     * 规则编码
     */
    private String ruleCode;

    /**
     * 描述
     */
    private String description;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 部署ID
     */
    private Long deployId;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateUser;

    /**
     * update_time
     */
    private Date updateTime;

    /**
     * 删除人
     */
    private String deleteUser;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 是否删除 0未删除 1删除
     */
    private Integer isDelete;

    /**
     * 0保存，1发布，2作废
     */
    private Integer dataStatus;

    /**
     * 发布人名称
     */
    private String publishUserName;

    /**
     * 发布时间
     */
    private Date publishTime;

    /**
     * 发布人
     */
    private String publishUser;


}
