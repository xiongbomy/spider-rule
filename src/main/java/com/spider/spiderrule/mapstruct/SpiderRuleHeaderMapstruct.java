package com.spider.spiderrule.mapstruct;

import com.spider.spiderrule.model.entity.SpiderRuleEditHeader;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishHeader;
import com.spider.spiderrule.model.entity.SpiderRulePublishHeader;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/16 8:28 下午
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface SpiderRuleHeaderMapstruct {

    SpiderRulePublishHeader toPublishHeader(SpiderRuleEditHeader editHeader);

    SpiderRuleHisPublishHeader toHisPublishHeader(SpiderRulePublishHeader publishHeader);

}
