package com.spider.spiderrule.mapstruct;

import com.spider.spiderrule.model.entity.SpiderRuleEditItem;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishItem;
import com.spider.spiderrule.model.entity.SpiderRulePublishItem;
import com.spider.spiderrule.model.vo.RulePublishItemVo;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/16 8:43 下午
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface SpiderRuleItemMapstruct {

    SpiderRulePublishItem toPublishItem(SpiderRuleEditItem editItem);

    SpiderRuleHisPublishItem toHisPublishItem(SpiderRulePublishItem publishItem);

    RulePublishItemVo toRulePublishItemVo(SpiderRulePublishItem publishItem);
}
