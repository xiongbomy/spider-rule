package com.spider.spiderrule.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.spider.spiderrule.common.ResponseEntity;
import com.spider.spiderrule.model.entity.SpiderRuleEditItem;
import com.spider.spiderrule.service.SpiderRuleEditItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/15 8:39 下午
 */

@Api(tags = "规则矩阵编辑表行控制器")
@Slf4j
@RestController
@RequestMapping("/rule-edit-item")
public class SpiderRuleEditItemController {

    @Autowired
    private SpiderRuleEditItemService spiderRuleEditItemService;

    @ApiOperation("编辑的规则矩阵表行-列表")
    @GetMapping("/list")
    public ResponseEntity<IPage<SpiderRuleEditItem>> selectPage(Long ruleId, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(spiderRuleEditItemService.selectPage(ruleId, page, pageSize));
    }

    @ApiOperation("编辑的规则矩阵表行-详情")
    @GetMapping("/{id}")
    public ResponseEntity<SpiderRuleEditItem> findById(@PathVariable Long id) {
        return ResponseEntity.ok(spiderRuleEditItemService.findById(id));
    }


    @ApiOperation("编辑的规则矩阵表行-批量新增")
    @PostMapping("/batchSave")
    public ResponseEntity<List<SpiderRuleEditItem>> batchSaveRuleEditHeader(@RequestBody List<SpiderRuleEditItem> ruleEditHeaders) {
        return ResponseEntity.ok(spiderRuleEditItemService.batchSaveRuleEditHeader(ruleEditHeaders));
    }

    @ApiOperation("编辑的规则矩阵表行-批量编辑")
    @PutMapping("/batchEdit")
    public ResponseEntity<List<SpiderRuleEditItem>> batchEditRuleEditHeader(@RequestBody List<SpiderRuleEditItem> ruleEditHeaders) {
        return ResponseEntity.ok(spiderRuleEditItemService.batchEditRuleEditHeader(ruleEditHeaders));
    }

    @ApiOperation("编辑的规则矩阵表行-批量删除")
    @DeleteMapping("/batchDelete")
    public ResponseEntity<Boolean> deleteRuleEditHeader(@RequestBody List<Long> ids) {
        return ResponseEntity.ok(spiderRuleEditItemService.deleteRuleEditHeader(ids));
    }

}
