package com.spider.spiderrule.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.spider.spiderrule.common.ResponseEntity;
import com.spider.spiderrule.model.entity.SpiderRuleDeploy;
import com.spider.spiderrule.model.vo.RuleHisPublishDataVo;
import com.spider.spiderrule.service.SpiderRuleDeployService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/5/4 11:34 上午
 */

@Api(tags = "规则发布记录控制器")
@Slf4j
@RestController
@RequestMapping("/spider-rule-deploy")
public class SpiderRuleDeployController {

    @Autowired
    private SpiderRuleDeployService ruleDeployService;

    @ApiOperation("查看单个规则的发布记录列表")
    @GetMapping("/ruleId")
    public ResponseEntity<IPage<SpiderRuleDeploy>> selectPage(@RequestParam Long ruleId, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(ruleDeployService.selectPage(ruleId, page, pageSize));
    }

    @ApiOperation("查看单个规则的历史发布记录")
    @GetMapping("/his")
    public ResponseEntity<RuleHisPublishDataVo> selectHisPublishList(@RequestParam Long ruleId) {
        return ResponseEntity.ok(ruleDeployService.selectHisPublishList(ruleId));
    }

}
