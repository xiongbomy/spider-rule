package com.spider.spiderrule.controller;

import com.alibaba.fastjson.JSONObject;
import com.spider.spiderrule.common.ResponseEntity;
import com.spider.spiderrule.model.param.RuleMatchParam;
import com.spider.spiderrule.service.SpiderRuleMatchApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/21 10:58 上午
 */

@Api(tags = "规则匹配API控制器")
@Slf4j
@RestController
@RequestMapping("/spider-rule/match")
public class SpiderRuleMatchApiController {

    @Autowired
    private SpiderRuleMatchApiService ruleMatchApiService;

    @ApiOperation("规则匹配")
    @PostMapping("/ruleCode")
    public ResponseEntity<JSONObject> matchRuleByRuleCode(@RequestBody RuleMatchParam ruleMatchParam) {
        return ResponseEntity.ok(ruleMatchApiService.matchRuleByRuleCode(ruleMatchParam.getRuleCode(), ruleMatchParam.getParameter()));
    }

}
