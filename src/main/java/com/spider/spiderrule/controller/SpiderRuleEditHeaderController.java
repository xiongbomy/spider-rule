package com.spider.spiderrule.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.spider.spiderrule.common.ResponseEntity;
import com.spider.spiderrule.model.entity.SpiderRuleEditHeader;
import com.spider.spiderrule.service.SpiderRuleEditHeaderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/13 11:35 上午
 */

@Api(tags = "规则矩阵编辑表头控制器")
@Slf4j
@RestController
@RequestMapping("/rule-edit-header")
public class SpiderRuleEditHeaderController {

    @Autowired
    private SpiderRuleEditHeaderService spiderRuleEditHeaderService;


    @ApiOperation("编辑的规则矩阵表头-列表")
    @GetMapping("/list")
    public ResponseEntity<IPage<SpiderRuleEditHeader>> selectPage(Long ruleId, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(spiderRuleEditHeaderService.selectPage(ruleId, page, pageSize));
    }

    @ApiOperation("编辑的规则矩阵表头-详情")
    @GetMapping("/{id}")
    public ResponseEntity<SpiderRuleEditHeader> findById(@PathVariable Long id) {
        return ResponseEntity.ok(spiderRuleEditHeaderService.findById(id));
    }


    @ApiOperation("编辑的规则矩阵表头-批量新增")
    @PostMapping("/batchSave")
    public ResponseEntity<List<SpiderRuleEditHeader>> batchSaveRuleEditHeader(@RequestBody List<SpiderRuleEditHeader> ruleEditHeaders) {
        return ResponseEntity.ok(spiderRuleEditHeaderService.batchSaveRuleEditHeader(ruleEditHeaders));
    }

    @ApiOperation("编辑的规则矩阵表头-批量编辑")
    @PutMapping("/batchEdit")
    public ResponseEntity<List<SpiderRuleEditHeader>> batchEditRuleEditHeader(@RequestBody List<SpiderRuleEditHeader> ruleEditHeaders) {
        return ResponseEntity.ok(spiderRuleEditHeaderService.batchEditRuleEditHeader(ruleEditHeaders));
    }

    @ApiOperation("编辑的规则矩阵表头-批量删除")
    @DeleteMapping("/batchDelete")
    public ResponseEntity<Boolean> deleteRuleEditHeader(@RequestBody List<Long> ids) {
        return ResponseEntity.ok(spiderRuleEditHeaderService.deleteRuleEditHeader(ids));
    }
}
