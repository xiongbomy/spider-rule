package com.spider.spiderrule.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.spider.spiderrule.common.ResponseEntity;
import com.spider.spiderrule.model.entity.SpiderRule;
import com.spider.spiderrule.service.SpiderRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 规则引擎-规则表控制器
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 8:27 下午
 */

@Api(tags = "规则表控制器")
@Slf4j
@RestController
@RequestMapping("/spider-rule")
public class SpiderRuleController {

    @Autowired
    private SpiderRuleService spiderRuleService;

    @ApiOperation("规则列表")
    @GetMapping
    public ResponseEntity<IPage<SpiderRule>> selectPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(spiderRuleService.selectPage(page, pageSize));
    }

    @ApiOperation("查询规则主表详情")
    @GetMapping("/{id}")
    public ResponseEntity<SpiderRule> findById(@PathVariable Long id) {
        return ResponseEntity.ok(spiderRuleService.findById(id));
    }


    @ApiOperation("新增规则主表")
    @PostMapping
    public ResponseEntity<SpiderRule> saveRule(@RequestBody SpiderRule spiderRule) {
        return ResponseEntity.ok(spiderRuleService.saveRule(spiderRule));
    }

    @ApiOperation("编辑规则主表")
    @PutMapping
    public ResponseEntity<SpiderRule> editRule(@RequestBody SpiderRule spiderRule) {
        return ResponseEntity.ok(spiderRuleService.editRule(spiderRule));
    }


    @ApiOperation("删除某条规则主表")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteRule(@PathVariable Long id) {
        return ResponseEntity.ok(spiderRuleService.deleteRule(id));
    }

    @ApiOperation("作废规则")
    @PostMapping("/invalid/{id}")
    public ResponseEntity<Boolean> invalidRule(@PathVariable Long id) {
        return ResponseEntity.ok(spiderRuleService.invalidRule(id));
    }

    @ApiOperation("取消作废规则")
    @PostMapping("/un-invalid/{id}")
    public ResponseEntity<Boolean> unInvalidRule(@PathVariable Long id) {
        return ResponseEntity.ok(spiderRuleService.unInvalidRule(id));
    }

    @ApiOperation("发布规则")
    @PostMapping("/release/{id}")
    public ResponseEntity<Boolean> releaseRule(@PathVariable Long id) {
        return ResponseEntity.ok(spiderRuleService.releaseRule(id));
    }

}