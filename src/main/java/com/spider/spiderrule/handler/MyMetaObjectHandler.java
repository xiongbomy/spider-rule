package com.spider.spiderrule.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.spider.spiderrule.config.BusinessConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 使用Mybatis-plus的自动填充处理器
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/15 8:50 下午
 */

@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Autowired
    private BusinessConfig businessConfig;

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "tenantId", String.class, businessConfig.getTenantId()); // 起始版本 3.3.0(推荐使用)
    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
