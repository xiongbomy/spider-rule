package com.spider.spiderrule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spider.spiderrule.model.entity.SpiderRuleEditHeader;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:18 下午
 */
@Mapper
public interface SpiderRuleEditHeaderMapper extends BaseMapper<SpiderRuleEditHeader> {


}
