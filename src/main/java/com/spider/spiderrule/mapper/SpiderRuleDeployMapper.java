package com.spider.spiderrule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spider.spiderrule.model.entity.SpiderRuleDeploy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:30 下午
 */

@Mapper
public interface SpiderRuleDeployMapper extends BaseMapper<SpiderRuleDeploy> {

    @Select("SELECT `version` FROM t_spider_rule_deploy WHERE rule_id = ${ruleId} ORDER BY version desc limit 1; ")
    int selectLatestVersion(@Param("ruleId") Long ruleId);

}
