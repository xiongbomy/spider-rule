package com.spider.spiderrule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spider.spiderrule.model.entity.SpiderRuleHisPublishItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:33 下午
 */

@Mapper
public interface SpiderRuleHisPublishItemMapper extends BaseMapper<SpiderRuleHisPublishItem> {
}
