package com.spider.spiderrule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spider.spiderrule.model.entity.SpiderRule;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/9 11:26 下午
 */

@Mapper
public interface SpiderRuleMapper extends BaseMapper<SpiderRule> {


}
