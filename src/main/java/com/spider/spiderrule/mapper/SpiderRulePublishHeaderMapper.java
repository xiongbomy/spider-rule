package com.spider.spiderrule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spider.spiderrule.model.entity.SpiderRulePublishHeader;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:49 下午
 */

@Mapper
public interface SpiderRulePublishHeaderMapper extends BaseMapper<SpiderRulePublishHeader> {

    @Delete("DELETE FROM t_spider_rule_publish_header WHERE rule_id = ${ruleId}")
    int deleteByRuleId(@Param("ruleId") Long ruleId);

}
