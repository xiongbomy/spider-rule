package com.spider.spiderrule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spider.spiderrule.model.entity.SpiderRuleEditItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/10 7:21 下午
 */
@Mapper
public interface SpiderRuleEditItemMapper extends BaseMapper<SpiderRuleEditItem> {
    
}
