package com.spider.spiderrule.enums;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/13 11:31 上午
 */
public enum RuleDataStatusEnum {

    SAVE(0, "保存"),
    PUBLISH(1, "发布"),
    INVALID(2, "作废");


    private final int code;

    private final String name;

    RuleDataStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}
