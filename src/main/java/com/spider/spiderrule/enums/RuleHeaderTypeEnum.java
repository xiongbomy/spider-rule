package com.spider.spiderrule.enums;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/4/21 7:56 下午
 */
public enum RuleHeaderTypeEnum {

    CONDITION(1, "condition"),
    RESULT(2, "result"),
    GROUPING(3, "grouping");

    private final int code;

    private final String name;

    RuleHeaderTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}
