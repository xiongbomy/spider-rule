# spider-rule

## 介绍
一款灵活且性能高效的规则引擎，采用Mysql+Redis+匹配算法实现。
灵感来源于Drools，但Drools将数据放到服务内存中，当数据量很大，服务所需要的内存空间也相当大，且不管有没有用到都需要加载到内存中，而且每次加载很慢。
因此设计一款将数据放到Mysql和Redis中，Mysql存储发布的规则数据，Redis存储最近使用的规则数据。

**功能介绍：**

支持基于规则的系统开发，可以将业务规则从应用程序代码中分离出来，使其更易于维护和修改。

**应用场景**

规则引擎的应用领域十分广泛，比如如下应用场景

1，决策支持系统：在金融、保险、医疗等行业中，Drools 可用于构建复杂的决策流程，比如金融行业的风控反欺诈系统、信贷审批、催收分案、支付路由等有广泛的使用

2，动态配置与策略管理：电信、电子商务等领域中，可以根据用户行为、市场条件等动态调整服务策略或营销活动规则。

3，合规性检查：在银行业、证券业和其他监管严格的行业，Drools 可用来实时验证交易是否符合法规要求。

4，物联网(IoT)智能决策：在处理大量传感器数据时，可以利用 Drools 实现基于规则的数据分析和实时决策响应。

**举例说明**

需求：
```batsh
商城系统消费赠送积分

100元以下, 不加分 
100元-500元 加100分 
500元-1000元 加500分 
1000元 以上 加1000分
......
```

传统做法
```batsh
if (order.getAmout() <= 100){
    order.setScore(0);
    addScore(order);
}else if(order.getAmout() > 100 && order.getAmout() <= 500){
    order.setScore(100);
    addScore(order);
}else if(order.getAmout() > 500 && order.getAmout() <= 1000){
    order.setScore(500);
    addScore(order);
}else{
    order.setScore(1000);
    addScore(order);
}
```

策略模式
```batsh
interface Strategy {
    addScore(int num1,int num2);
}

class Strategy1 {
    addScore(int num1);
}
......................
interface StrategyN {
    addScore(int num1);
}

class Environment {
    private Strategy strategy;

    public Environment(Strategy strategy) {
        this.strategy = strategy;
    }

    public int addScore(int num1) {
        return strategy.addScore(num1);
    }
}
```

遇到的问题
```batsh
以上解决方法问题思考：

如果需求变更，积分层次结构增加，积分比例调整？

数据库？

遇到的问题瓶颈：

第一，我们要简化if else结构,让业务逻辑和数据分离！

第二，分离出的业务逻辑必须要易于编写，至少单独编写这些业务逻辑，要比写代码快！

第三，分离出的业务逻辑必须要比原来的代码更容易读懂！

第四，分离出的业务逻辑必须比原来的易于维护，至少改动这些逻辑，应用程序不用重启！
```

解决方案
```batsh
使用规则引擎来将业务规则从应用程序代码中分离出来，使其更易于维护和修改。
开源的Java四大规则引擎有Drools、URule、Groovy、Aviator。

本项目spider-rule也是开源的Java开发的轻量级的规则引擎。
```

### spider-rule和Drools的对比

1，数据存储方式不同

Drools将配置的规则数据都加载到服务内存中。

spider-rule将配置的规则数据加载到Redis中。

2，Drools的优缺点

优点：性能高，功能较齐全

缺点：业务数据一多，服务内存会越来越到，一般服务内存2-3GB，但业务数据多，可能服务内容占用超过30GB都有，占用资源。

3，spider-rule的优缺点

优点：数据以极简的内容存到Redis，不占用服务本身的内存空间。

缺点：功能简单，不齐全。

## 软件架构
Springboot,Mysql,Mybatis-plus,Redis.


1，规则主表相关接口

1. 新增规则
2. 编辑规则
3. 删除规则
4. 查看规则详情
5. 查看规则列表
6. 发布规则
7. 作废规则
8. 取消作废规则


2，规则矩阵表头相关接口

1. 批量新增规则矩阵表头
2. 批量编辑规则矩阵表头
3. 批量删除规则矩阵表头
4. 查看规则矩阵表头列表
5. 查看规则矩阵表头详情


3，规则矩阵表行相关接口

1. 批量新增规则矩阵表行
2. 批量编辑规则矩阵表行
3. 批量删除规则矩阵表行
4. 查看规则矩阵表行列表
5. 查看规则矩阵表行详情


4，规则发布记录相关接口

1. 查看单个规则的发布记录列表
2. 查看单个规则的历史发布记录

5，规则匹配接口

#### swagger接口图

![img.png](src/main/resources/static/swagger-img.png)

#### excel来表达表头和表行，输入和输出的关系图
![img.png](src/main/resources/static/excel-img.png)

#### JMeter接口性能测试
![img_1.png](src/main/resources/static/jemeter-img2.png)
![img.png](src/main/resources/static/jemeter-img.png)


数据库表结构设计：
![img.png](src/main/resources/static/mysql-img.png)

## 安装教程

1. 克隆项目到本地
   git clone https://gitee.com/xiongbomy/spider-rule.git
2. 修改application.yml下的mysql数据库连接和Redis连接
3. 启动服务

## 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
