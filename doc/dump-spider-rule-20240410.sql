-- MySQL dump 10.13  Distrib 8.0.26, for macos11 (arm64)
--
-- Host: localhost    Database: spider-rule
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_spider_rule`
--

DROP TABLE IF EXISTS `t_spider_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_spider_rule` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '租户号',
  `rule_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '规则名称',
  `rule_code` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '规则编码',
  `description` varchar(512) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '描述',
  `version` int DEFAULT '0' COMMENT '版本号',
  `deploy_id` bigint DEFAULT '-1' COMMENT '部署ID',
  `create_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建人',
  `create_user_name` varchar(32) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建人名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `delete_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '删除人',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `revision` int NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0未删除 1删除',
  `data_status` int NOT NULL DEFAULT '0' COMMENT '0保存，1发布，2作废',
  `publish_user_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '发布人名称',
  `publish_time` datetime DEFAULT '2000-01-01 00:00:00' COMMENT '发布时间',
  `publish_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '发布人',
  PRIMARY KEY (`id`),
  KEY `IDX_DR_TENANT_ID` (`tenant_id`),
  KEY `IDX_DR_RULE_CODE` (`rule_code`),
  KEY `IDX_DR_DEPLOY_ID` (`deploy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='规则引擎-规则表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_spider_rule`
--

LOCK TABLES `t_spider_rule` WRITE;
/*!40000 ALTER TABLE `t_spider_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_spider_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_spider_rule_deploy`
--

DROP TABLE IF EXISTS `t_spider_rule_deploy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_spider_rule_deploy` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户号',
  `rule_id` bigint DEFAULT NULL COMMENT '规则表ID',
  `version` int DEFAULT NULL COMMENT '版本号',
  `create_user` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_user_name` varchar(60) DEFAULT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `IDX_DD_TENANT_ID` (`tenant_id`),
  KEY `IDX_DD_RULE_ID` (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='规则引擎-规则发布记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_spider_rule_deploy`
--

LOCK TABLES `t_spider_rule_deploy` WRITE;
/*!40000 ALTER TABLE `t_spider_rule_deploy` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_spider_rule_deploy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_spider_rule_edit_header`
--

DROP TABLE IF EXISTS `t_spider_rule_edit_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_spider_rule_edit_header` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '租户号',
  `header_uuid` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '头表标识',
  `field_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字段名称',
  `field_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字段编码',
  `header_index` int DEFAULT '0' COMMENT '头顺序',
  `rule_id` bigint DEFAULT '0' COMMENT '规则编码',
  `rule_code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '规则编码',
  `header_type` tinyint(1) DEFAULT '0' COMMENT '类型:1-输入,2-输出,3-分组',
  `create_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `delete_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '删除人',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `revision` int NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0未删除 1删除',
  PRIMARY KEY (`id`),
  KEY `IDX_DEH_TENANT_ID` (`tenant_id`) USING BTREE,
  KEY `IDX_DEH_RULE_ID` (`rule_id`) USING BTREE,
  KEY `IDX_DEH_RULE_CODE` (`rule_code`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='发布的规则规则矩阵-头表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_spider_rule_edit_header`
--

LOCK TABLES `t_spider_rule_edit_header` WRITE;
/*!40000 ALTER TABLE `t_spider_rule_edit_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_spider_rule_edit_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_spider_rule_edit_item`
--

DROP TABLE IF EXISTS `t_spider_rule_edit_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_spider_rule_edit_item` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '租户号',
  `header_uuid` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '头表标识',
  `row_index` int DEFAULT '0' COMMENT '行号',
  `operator` text COLLATE utf8mb4_general_ci COMMENT '操作符',
  `rule_id` bigint DEFAULT '0' COMMENT '规则ID',
  `rule_code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '规则编码',
  `create_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `delete_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '删除人',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `revision` int NOT NULL DEFAULT '0' COMMENT '乐观锁',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0未删除 1删除',
  PRIMARY KEY (`id`),
  KEY `IDX_DEI_TENANT_ID` (`tenant_id`) USING BTREE,
  KEY `IDX_DEI_RULE_ID` (`rule_id`) USING BTREE,
  KEY `IDX_DEI_RULE_CODE` (`rule_code`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='发布的规则规则矩阵-行表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_spider_rule_edit_item`
--

LOCK TABLES `t_spider_rule_edit_item` WRITE;
/*!40000 ALTER TABLE `t_spider_rule_edit_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_spider_rule_edit_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_spider_rule_his_publish_header`
--

DROP TABLE IF EXISTS `t_spider_rule_his_publish_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_spider_rule_his_publish_header` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '租户号',
  `version` int DEFAULT '1' COMMENT '当前最新版本',
  `header_uuid` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '头表标识',
  `field_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字段名称',
  `field_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字段编码',
  `header_index` int DEFAULT '0' COMMENT '头顺序',
  `rule_id` bigint DEFAULT '0' COMMENT '规则编码',
  `rule_code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '规则编码',
  `header_type` tinyint(1) DEFAULT '0' COMMENT '类型:1-输入,2-输出,3-分组',
  `create_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  KEY `IDX_DEH_TENANT_ID` (`tenant_id`) USING BTREE,
  KEY `IDX_DEH_RULE_ID` (`rule_id`) USING BTREE,
  KEY `IDX_DEH_RULE_CODE` (`rule_code`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='发布的规则规则矩阵-头表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_spider_rule_his_publish_header`
--

LOCK TABLES `t_spider_rule_his_publish_header` WRITE;
/*!40000 ALTER TABLE `t_spider_rule_his_publish_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_spider_rule_his_publish_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_spider_rule_his_publish_item`
--

DROP TABLE IF EXISTS `t_spider_rule_his_publish_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_spider_rule_his_publish_item` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '租户号',
  `version` int DEFAULT '1' COMMENT '当前最新版本',
  `header_uuid` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '头表标识',
  `row_index` int DEFAULT '0' COMMENT '行号',
  `operator` text COLLATE utf8mb4_general_ci COMMENT '操作符',
  `rule_id` bigint DEFAULT '0' COMMENT '规则ID',
  `rule_code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '规则编码',
  `create_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  KEY `IDX_DEI_TENANT_ID` (`tenant_id`) USING BTREE,
  KEY `IDX_DEI_RULE_ID` (`rule_id`) USING BTREE,
  KEY `IDX_DEI_RULE_CODE` (`rule_code`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='发布的规则规则矩阵-行表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_spider_rule_his_publish_item`
--

LOCK TABLES `t_spider_rule_his_publish_item` WRITE;
/*!40000 ALTER TABLE `t_spider_rule_his_publish_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_spider_rule_his_publish_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_spider_rule_publish_header`
--

DROP TABLE IF EXISTS `t_spider_rule_publish_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_spider_rule_publish_header` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '租户号',
  `data_status` int DEFAULT '0' COMMENT '数据状态,1发布，2作废',
  `version` int DEFAULT '1' COMMENT '当前最新版本',
  `header_uuid` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '头表标识',
  `field_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字段名称',
  `field_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字段编码',
  `header_index` int DEFAULT '0' COMMENT '头顺序',
  `rule_id` bigint DEFAULT '0' COMMENT '规则编码',
  `rule_code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '规则编码',
  `header_type` tinyint(1) DEFAULT '0' COMMENT '类型:1-输入,2-输出,3-分组',
  `create_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  KEY `IDX_DEH_TENANT_ID` (`tenant_id`) USING BTREE,
  KEY `IDX_DEH_RULE_ID` (`rule_id`) USING BTREE,
  KEY `IDX_DEH_RULE_CODE` (`rule_code`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='发布的规则规则矩阵-头表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_spider_rule_publish_header`
--

LOCK TABLES `t_spider_rule_publish_header` WRITE;
/*!40000 ALTER TABLE `t_spider_rule_publish_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_spider_rule_publish_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_spider_rule_publish_item`
--

DROP TABLE IF EXISTS `t_spider_rule_publish_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_spider_rule_publish_item` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '租户号',
  `data_status` int DEFAULT '0' COMMENT '数据状态,1发布，2作废',
  `version` int DEFAULT '1' COMMENT '当前最新版本',
  `header_uuid` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '头表标识',
  `row_index` int DEFAULT '0' COMMENT '行号',
  `operator` text COLLATE utf8mb4_general_ci COMMENT '操作符',
  `rule_id` bigint DEFAULT '0' COMMENT '规则ID',
  `rule_code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '规则编码',
  `create_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  KEY `IDX_DEI_TENANT_ID` (`tenant_id`) USING BTREE,
  KEY `IDX_DEI_RULE_ID` (`rule_id`) USING BTREE,
  KEY `IDX_DEI_RULE_CODE` (`rule_code`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='发布的规则规则矩阵-行表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_spider_rule_publish_item`
--

LOCK TABLES `t_spider_rule_publish_item` WRITE;
/*!40000 ALTER TABLE `t_spider_rule_publish_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_spider_rule_publish_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'spider-rule'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-10 20:17:46
